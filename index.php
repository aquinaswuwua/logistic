<?php

require_once (__DIR__ . '/src/Class/Product/AgriculturalDevice.php');
require_once (__DIR__ . '/src/Class/Product/Package.php');
require_once (__DIR__ . '/src/Class/MeanOfTransport/Lorry.php');
require_once (__DIR__ . '/src/Class/MeanOfTransport/Plane.php');
require_once (__DIR__ . '/src/Class/Decorator/SortedByWeightLorryDecorator.php');
require_once (__DIR__ . '/src/Class/MeanOfTransport/Truck.php');
require_once (__DIR__ . '/src/Class/Factory/TransporterFactory.php');
require_once (__DIR__ . '/src/Class/Factory/ProductFactory.php');
require_once (__DIR__ . '/src/Class/ShipmentAssigner.php');
require_once (__DIR__ . '/src/Class/ShipmentLoader.php');
require_once (__DIR__ . '/src/Class/Hydrator/GoodHydrator.php');


if (isset($argv[1]) && isset($argv[2])) {
    $meanOfTransportToReloadGoods = $argv[1];
    $transportDetails = unserialize(urldecode($argv[2]));
} else if (isset($_GET['meanOfTransportToReloadGoods']) &&
    !is_null($_GET['meanOfTransportToReloadGoods']) &&
    isset($_GET['transportDetails']) &
    !is_null($_GET['transportDetails'])
    ) {
    $meanOfTransportToReloadGoods = $_GET['meanOfTransportToReloadGoods'];
    $transportDetails = unserialize(urldecode($_GET['transportDetails']));
}

if (!$transportDetails || !$meanOfTransportToReloadGoods) {
    die('Missing data');
}

$productFactory = new ProductFactory();

$goodHydrator = new GoodHydrator($productFactory);

$lorry = new Lorry();

foreach ($transportDetails as $singleProductDetails) {
    $product = $goodHydrator->hydarateProduct($singleProductDetails);
    $lorry->setShipments($product);
}

$transportFacotry = new TransporterFactory();

$transporter = $transportFacotry->buildTransporter($meanOfTransportToReloadGoods);

$transporterWithShipmentSortedByWeight = new SortedByWeightLorryDecorator($lorry);

$goodsShippmentManager = new ShipmentAssigner($transporter, $transporterWithShipmentSortedByWeight);

$goodsAssinged = $goodsShippmentManager->manageShipment();

$shipmentLoader = new ShipmentLoader();

$report = $shipmentLoader
    ->setFactory($transportFacotry)
    ->setGoods($goodsAssinged)
    ->setMeanOfTransport($meanOfTransportToReloadGoods)
    ->loadGoods()
;

var_dump($report);die;

//WORKING EXAPLE
//
//$lorry1 = new Lorry();
//
//$packed1 = new Package();
//$packed1->setId(1);
//$packed1->setWeight(200);
//$lorry1->setShipments($packed1);
//
//$packed2 = new Package();
//$packed2->setId(2);
//$packed2->setWeight(rand(10, 20));
//$lorry1->setShipments($packed2);
//
//$packed3 = new Package();
//$packed3->setId(3);
//$packed3->setWeight(rand(10, 20));
//$lorry1->setShipments($packed3);
//
//$packed4 = new Package();
//$packed4->setId(4);
//$packed4->setWeight(rand(10, 20));
//$lorry1->setShipments($packed4);
//
//$packed5 = new Package();
//$packed5->setId(5);
//$packed5->setWeight(rand(10, 20));
//$lorry1->setShipments($packed5);
//
//$packed6 = new Package();
//$packed6->setId(6);
//$packed6->setWeight(rand(10, 20));
//$lorry1->setShipments($packed6);
//
//$packed7 = new Package();
//$packed7->setId(7);
//$packed7->setWeight(rand(10, 20));
//$lorry1->setShipments($packed7);
//
//$packed8 = new Package();
//$packed8->setId(8);
//$packed8->setWeight(rand(10, 20));
//$lorry1->setShipments($packed8);
//
//$packed9 = new Package();
//$packed9->setId(9);
//$packed9->setWeight(rand(10, 20));
//$lorry1->setShipments($packed9);
//
//$packed10 = new Package();
//$packed10->setId(10);
//$packed10->setWeight(rand(10, 20));
//$lorry1->setShipments($packed10);
//
//$packed11 = new Package();
//$packed11->setId(1);
//$packed11->setWeight(rand(10, 20));
//$lorry1->setShipments($packed11);
//
//$packed12 = new Package();
//$packed12->setId(12);
//$packed12->setWeight(rand(10, 20));
//$lorry1->setShipments($packed12);
//
//$packed13 = new Package();
//$packed13->setId(13);
//$packed13->setWeight(rand(10, 20));
//$lorry1->setShipments($packed12);
//
//$packed14 = new Package();
//$packed14->setId(14);
//$packed14->setWeight(rand(10, 20));
//$lorry1->setShipments($packed14);
//
//$packed15 = new Package();
//$packed15->setId(15);
//$packed15->setWeight(rand(10, 20));
//$lorry1->setShipments($packed15);
//
//$packed16 = new Package();
//$packed16->setId(16);
//$packed16->setWeight(rand(10, 20));
//$lorry1->setShipments($packed16);
//
//$packed17 = new Package();
//$packed17->setId(17);
//$packed17->setWeight(rand(10, 20));
//$lorry1->setShipments($packed17);
//
//$packed18 = new Package();
//$packed18->setId(18);
//$packed18->setWeight(rand(10, 20));
//$lorry1->setShipments($packed18);
//
//$packed19 = new Package();
//$packed19->setId(19);
//$packed19->setWeight(rand(10, 20));
//$lorry1->setShipments($packed19);
//
//$packed20 = new Package();
//$packed20->setId(1);
//$packed20->setWeight(rand(10, 20));
//$lorry1->setShipments($packed20);
//
//$packed1 = new Package();
//$packed1->setId(1);
//$packed1->setWeight(rand(10, 20));
//$lorry1->setShipments($packed1);
//
//$packed2 = new Package();
//$packed2->setId(2);
//$packed2->setWeight(rand(10, 20));
//$lorry1->setShipments($packed2);
//
//$packed3 = new Package();
//$packed3->setId(3);
//$packed3->setWeight(rand(10, 20));
//$lorry1->setShipments($packed3);
//
//$packed4 = new Package();
//$packed4->setId(4);
//$packed4->setWeight(rand(10, 20));
//$lorry1->setShipments($packed4);
//
//$packed5 = new Package();
//$packed5->setId(5);
//$packed5->setWeight(rand(10, 20));
//$lorry1->setShipments($packed5);
//
//$packed6 = new Package();
//$packed6->setId(6);
//$packed6->setWeight(rand(10, 20));
//$lorry1->setShipments($packed6);
//
//$packed7 = new Package();
//$packed7->setId(7);
//$packed7->setWeight(rand(10, 20));
//$lorry1->setShipments($packed7);
//
//$packed8 = new Package();
//$packed8->setId(8);
//$packed8->setWeight(rand(10, 20));
//$lorry1->setShipments($packed8);
//
//$packed9 = new Package();
//$packed9->setId(9);
//$packed9->setWeight(rand(10, 20));
//$lorry1->setShipments($packed9);
//
//$packed10 = new Package();
//$packed10->setId(10);
//$packed10->setWeight(rand(10, 20));
//$lorry1->setShipments($packed10);
//
//$packed11 = new Package();
//$packed11->setId(1);
//$packed11->setWeight(rand(10, 20));
//$lorry1->setShipments($packed11);
//
//$packed12 = new Package();
//$packed12->setId(12);
//$packed12->setWeight(rand(10, 20));
//$lorry1->setShipments($packed12);
//
//$packed13 = new Package();
//$packed13->setId(13);
//$packed13->setWeight(rand(10, 20));
//$lorry1->setShipments($packed12);
//
//$packed14 = new Package();
//$packed14->setId(14);
//$packed14->setWeight(rand(10, 20));
//$lorry1->setShipments($packed14);
//
//$packed15 = new Package();
//$packed15->setId(15);
//$packed15->setWeight(rand(10, 20));
//$lorry1->setShipments($packed15);
//
//$packed16 = new Package();
//$packed16->setId(16);
//$packed16->setWeight(rand(10, 20));
//$lorry1->setShipments($packed16);
//
//$packed17 = new Package();
//$packed17->setId(17);
//$packed17->setWeight(rand(10, 20));
//$lorry1->setShipments($packed17);
//
//$packed18 = new Package();
//$packed18->setId(18);
//$packed18->setWeight(rand(10, 20));
//$lorry1->setShipments($packed18);
//
//$packed19 = new Package();
//$packed19->setId(19);
//$packed19->setWeight(rand(10, 20));
//$lorry1->setShipments($packed19);
//
//$packed20 = new Package();
//$packed20->setId(1);
//$packed20->setWeight(rand(10, 20));
//$lorry1->setShipments($packed20);
//
//$packed1 = new Package();
//$packed1->setId(1);
//$packed1->setWeight(rand(10, 20));
//$lorry1->setShipments($packed1);
//
//$packed2 = new Package();
//$packed2->setId(2);
//$packed2->setWeight(rand(10, 20));
//$lorry1->setShipments($packed2);
//
//$packed3 = new Package();
//$packed3->setId(3);
//$packed3->setWeight(rand(10, 20));
//$lorry1->setShipments($packed3);
//
//$packed4 = new Package();
//$packed4->setId(4);
//$packed4->setWeight(rand(10, 20));
//$lorry1->setShipments($packed4);
//
//$packed5 = new Package();
//$packed5->setId(5);
//$packed5->setWeight(rand(10, 20));
//$lorry1->setShipments($packed5);
//
//$packed6 = new Package();
//$packed6->setId(6);
//$packed6->setWeight(rand(10, 20));
//$lorry1->setShipments($packed6);
//
//$packed7 = new Package();
//$packed7->setId(7);
//$packed7->setWeight(rand(10, 20));
//$lorry1->setShipments($packed7);
//
//$packed8 = new Package();
//$packed8->setId(8);
//$packed8->setWeight(rand(10, 20));
//$lorry1->setShipments($packed8);
//
//$packed9 = new Package();
//$packed9->setId(9);
//$packed9->setWeight(rand(10, 20));
//$lorry1->setShipments($packed9);
//
//$packed10 = new Package();
//$packed10->setId(10);
//$packed10->setWeight(rand(10, 20));
//$lorry1->setShipments($packed10);
//
//$packed11 = new Package();
//$packed11->setId(1);
//$packed11->setWeight(rand(10, 20));
//$lorry1->setShipments($packed11);
//
//$packed12 = new Package();
//$packed12->setId(12);
//$packed12->setWeight(rand(10, 20));
//$lorry1->setShipments($packed12);
//
//$packed13 = new Package();
//$packed13->setId(13);
//$packed13->setWeight(rand(10, 20));
//$lorry1->setShipments($packed12);
//
//$packed14 = new Package();
//$packed14->setId(14);
//$packed14->setWeight(rand(10, 20));
//$lorry1->setShipments($packed14);
//
//
//$lorryDecorator = new SortedByWeightLorryDecorator($lorry1);
//
//$lorry2 = new Lorry();
//
//$agricMachine1 = new AgriculturalDevice();
//$agricMachine1->setId(1);
//$agricMachine1->setWeight(1500);
//$lorry2->setShipments($agricMachine1);
//
//$agricMachine2 = new AgriculturalDevice();
//$agricMachine2->setId(2);
//$agricMachine2->setWeight(2000);
//$lorry2->setShipments($agricMachine2);
//
//$truck = new Truck();
//
//$packagesShippmentManager = new ShipmentAssigner($truck, $lorryDecorator);
//$packagesAssinge = $packagesShippmentManager->manageShipment();
//
//
//$plane = new Plane();
//
//$agricMaichinesShippmentManager = new ShipmentAssigner($plane, $lorry2);
//
//$agricMachinesAssigned = $agricMaichinesShippmentManager->manageShipment();
//
//$shipmentLoader = new ShipmentLoader();
//
//$transportFacotry = new TransporterFactory();
//
//$truckReport = $shipmentLoader
//    ->setFactory($transportFacotry)
//    ->setGoods($packagesAssinge)
//    ->setMeanOfTransport('truck')
//    ->loadGoods()
//    ;
//
//$planeReport = $shipmentLoader
//    ->setFactory($transportFacotry)
//    ->setGoods($agricMachinesAssigned)
//    ->setMeanOfTransport('plane')
//    ->loadGoods()
//    ;
//

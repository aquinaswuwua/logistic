<?php

class ShipmentAssigner
{
    const FIRST_MEAN_OF_TRANSPORT_TO_LOAD_INDEX = 1;

    const INITIAL_SHIPMENT_WEIGHT = 0;

    /**
     * @var Truck
     */
    private $meanOfTransport;

    /**
     * @var Lorry
     */
    private $lorry ;

    private $result;

    private $recursivenessStop = false;

    /**
     * ShipmentAssigner constructor.
     *
     * @param Truck $meanOfTransport
     * @param Lorry $lorry
     */
    public function __construct(TransporterInterface $meanOfTransport, LorryInterface $lorry)
    {
        $this->meanOfTransport = $meanOfTransport;
        $this->lorry = $lorry;
    }

    public function manageShipment(): array
    {
        $this->assignShipmentsToMeanOfTransport(
            $this->lorry->getShipments(),
            $this->meanOfTransport,
            self::FIRST_MEAN_OF_TRANSPORT_TO_LOAD_INDEX
        );
        return $this->result;
    }

    private function assignShipmentsToMeanOfTransport($items, $meanOfTransport, $meanOfTransportIndex)
    {
        $totalWeightOfLoadedItems = self::INITIAL_SHIPMENT_WEIGHT;

        foreach ($items as $key => $item) {

            $totalWeightOfLoadedItems += $item->getWeight();

            if ($totalWeightOfLoadedItems > $meanOfTransport->getLoadMax()) {
                $meanOfTransportIndex++;

                $this->assignShipmentsToMeanOfTransport($items, $meanOfTransport, $meanOfTransportIndex);
            }

            if ($this->recursivenessStop) {
                return;
            }

            $this->result[$meanOfTransportIndex][] = $item;

            unset($items[$key]);

            if (0 == count($items)) {
                $this->recursivenessStop = true;
            }
        }
    }
}

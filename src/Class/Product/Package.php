<?php

require_once(__DIR__ . '/../../Interface/WeightableInterface.php');

class Package implements WeightableInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $weight;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }
}

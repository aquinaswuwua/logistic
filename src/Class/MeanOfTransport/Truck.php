<?php

require_once(__DIR__ . '/../../Interface/TransporterInterface.php');

class Truck implements TransporterInterface
{
    const MAX_LOAD = 200;

    private $goods;

    public function getLoadMax(): int
    {
        return self::MAX_LOAD;
    }

    /**
     * @return mixed
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @param mixed $goods
     */
    public function setGoods(array $goods): void
    {
        $this->goods = $goods;
    }
}

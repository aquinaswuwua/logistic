<?php

require_once(__DIR__ . '/../../Interface/LorryInterface.php');

class Lorry implements LorryInterface
{
    private $shipments = [];

    /**
     * @return array
     */
    public function getShipments(): array
    {
        return $this->shipments;
    }

    public function setShipments(WeightableInterface $shipments): void
    {
        $this->shipments[] = $shipments;
    }
}

<?php

require_once(__DIR__ . '/../../Interface/TransporterInterface.php');

class Plane implements TransporterInterface
{
    const MAX_LOAD = 2000;

    public function getLoadMax(): int
    {
        return self::MAX_LOAD;
    }

    /**
     * @return mixed
     */
    public function getGoods()
    {
        return $this->goods;
    }

    /**
     * @param mixed $goods
     */
    public function setGoods(array $goods): void
    {
        $this->goods = $goods;
    }
}

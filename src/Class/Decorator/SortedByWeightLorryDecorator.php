<?php

require_once(__DIR__ . '/../../Interface/LorryInterface.php');

class SortedByWeightLorryDecorator implements LorryInterface
{
    /**
     * @var Lorry
     */
    private $lorry;

    private $shipments = [];

    /**
     * SortedByWeightLorryDecorator constructor.
     *
     * @param Lorry $lorry
     */
    public function __construct(Lorry $lorry)
    {
        $this->lorry = $lorry;
    }

    /**
     * @return array
     */
    public function getShipments(): array
    {
        $this->shipments = $this->getLorry()->getShipments();

        $this->sortShipmentByWeight();

        return $this->shipments;
    }

    private function sortShipmentByWeight(): array
    {
        usort($this->shipments, function ($a, $b) {
            return $b->getWeight() <=> $a->getWeight();
        });

        return $this->shipments;
    }

    /**
     * @return Lorry
     */
    public function getLorry(): Lorry
    {
        return $this->lorry;
    }


}
<?php

require_once(__DIR__ . '/Factory/TransporterFactory.php');


class ShipmentLoader
{

    private $factory;

    private $goods;

    private $meanOfTransport;

    public function loadGoods()
    {
        $report = [];

        foreach ($this->goods as $singleShippment) {
            $transporter = $this->factory->buildTransporter($this->meanOfTransport);

            $transporter->setGoods($singleShippment);

            $report[] = $transporter;
        }

        return $report;
    }

    public function setFactory(TransporterFactory $factory): self
    {
        $this->factory = $factory;

        return $this;
    }

    public function setGoods(array $goods): self
    {
        $this->goods = $goods;

        return $this;
    }

    public function setMeanOfTransport(string $meanOfTransport): self
    {
        $this->meanOfTransport = $meanOfTransport;

        return $this;
    }
}

<?php

require_once(__DIR__ . '/../MeanOfTransport/Truck.php');
require_once(__DIR__ . '/../MeanOfTransport/Plane.php');


class TransporterFactory
{
    public function buildTransporter($name)
    {
        switch ($name):
            case 'truck':
                return new Truck();
                break;
            case 'plane':
                return new Plane();
                break;
            default:
                return false;
        endswitch;
    }
}

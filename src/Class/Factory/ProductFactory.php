<?php

require_once(__DIR__ . '/../Product/Package.php');
require_once(__DIR__ . '/../Product/AgriculturalDevice.php');

class ProductFactory
{
    public function buildProduct($name)
    {
        switch ($name):
            case 'Package':
                return new Package();
                break;
            case 'AgriculturalDevice':
                return new AgriculturalDevice();
                break;
            default:
                return false;
        endswitch;
    }
}

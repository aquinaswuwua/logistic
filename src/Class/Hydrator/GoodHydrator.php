<?php

class GoodHydrator
{
    private $productFactory;

    /**
     * ProductFactory constructor.
     *
     * @param $productFactory
     */
    public function __construct(ProductFactory $productFactory)
    {
        $this->productFactory = $productFactory;
    }

    public function hydarateProduct(
        string $productName,
        WeightableInterface $details
    ) {
        $product = $this->productFactory->buildProduct($productName);

        $product->setId($details['id']);
        $product->setWeight($details['weight']);

        return $product;
    }
}

<?php

interface WeightableInterface
{
    public function getWeight(): int;
}

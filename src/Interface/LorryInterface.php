<?php

interface LorryInterface
{
    public function getShipments(): array;
}

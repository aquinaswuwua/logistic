<?php

interface TransporterInterface
{
    public function getLoadMax(): int;

    public function setGoods(array $goods);
}
